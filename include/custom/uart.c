//
// Created by Shidfar Hodizoda on 14/04/2018.
//

#ifndef __AVR_ATmega32U4__
    #define __AVR_ATmega32U4__
#endif

#include <avr/io.h>
#include <stdio.h>

#ifndef F_CPU
    #define F_CPU 16000000UL
#endif

#ifndef BAUD
    #define BAUD  9600
#endif

#include <util/setbaud.h>
#include "uart.h"

void initialize_uart(void) {
    UBRR1H = UBRRH_VALUE;
    UBRR1L = UBRRL_VALUE;
#if USE_2X
    UCSR1A |= _BV(U2X1);
#else
    UCSR1A &= ~(_BV(U2X1));
#endif

    UCSR1C = _BV(UCSZ11) | _BV(UCSZ10); /* 8-bit data */
    UCSR1B = _BV(RXEN1) | _BV(TXEN1);   /* Enable RX and TX */
}

int uart_putchar(char c, FILE *stream) {
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    loop_until_bit_is_set(UCSR1A, UDRE1);
    UDR1 = c;
    return 0;
}

int uart_getchar(FILE *stream) {
    loop_until_bit_is_set(UCSR1A, RXC1); /* Wait until data exists. */
    return UDR1;
}
