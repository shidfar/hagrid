#ifndef STACKMON_H
    #define STACKMON_H

    #include <stdint.h>

    #if !defined(ON_PC)
        uint16_t StackCount(void);
    #else
        #define StackCount()    0
    #endif

#endif
