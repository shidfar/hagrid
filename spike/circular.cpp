#include <iostream>
#include <stdio.h>

#include "Circle.hpp"

#define NUM 21

int main(int argc, char const *argv[]) {
    Circle * circle = new Circle(0);
    Circle * first;
    first = circle;
    for (uint32_t i = 1; i < NUM; i ++) {
        Circle * next = new Circle(i);
        next->prev = circle;
        circle->next = next;
        circle = next;
    }
    circle->next = first;
    circle->next->prev = circle;
    circle = circle->next;

    for (auto i = 0; i < 42; i++) {
        printf("%d ", circle->idx);
        circle = circle->next;
    }
    printf("\n");


    for (auto i = 0; i < 42; i++) {
        printf("%d ", circle->idx);
        circle = circle->prev;
    }
    printf("\n");
    return 0;
}
