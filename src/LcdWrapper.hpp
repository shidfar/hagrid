#ifndef LCD_WRAPPER_H
    #define LCD_WRAPPER_H

    #include "GlobalDefs.h"
    #include "DevDefs.h"
    #include <Wire.h>
    #include <Adafruit_GFX.h>
    #include <Adafruit_SSD1306.h>

    namespace hagrid {
        class LcdWrapper
        {
        private:
            Adafruit_SSD1306 * oled;
            
        public:
            LcdWrapper();
            ~LcdWrapper();
            void mark_line(uint8_t);
            void clear_mark(uint8_t);
            void println(char *);
            void print(char *);
            void reset();
        };
    }
#endif
