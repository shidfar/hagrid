#ifndef KEYBOARD_HID_H
    #define KEYBOARD_HID_H

    #include "DevDefs.h"

    #include <Keyboard.h>

    namespace hagrid {
        class KeyboardHid {
        private:
            
        public:
            KeyboardHid();
            ~KeyboardHid();
            void type_in(char *);
        };
    }

#endif
