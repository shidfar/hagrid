#include <stdio.h>

#include "DevDefs.h" // only for dev purposes

#include <util/delay.h>
#include <avr/io.h>
#include <util/setbaud.h>

#ifdef __cplusplus
    extern "C" {
        FILE * uart_str_stream;
    }
#endif

void initialize_uart(void);
int uart_putchar(char, FILE *);
int uart_getchar(FILE *);

void initialize_uart(void) {
    UBRR1H = UBRRH_VALUE;
    UBRR1L = UBRRL_VALUE;

    #if USE_2X
        UCSR1A |= _BV(U2X1);
    #else
        UCSR1A &= ~(_BV(U2X1));
    #endif

    UCSR1C = _BV(UCSZ11) | _BV(UCSZ10); /* 8-bit data */
    UCSR1B = _BV(RXEN1) | _BV(TXEN1);   /* Enable RX and TX */

    uart_str_stream = fdevopen(uart_putchar, uart_getchar);
    stdout = stdin = uart_str_stream;
}

int uart_putchar(char c, FILE *stream) {
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    loop_until_bit_is_set(UCSR1A, UDRE1);
    UDR1 = c;
    return 0;
}

int uart_getchar(FILE *stream) {
    loop_until_bit_is_set(UCSR1A, RXC1); /* Wait until data exists. */
    return UDR1;
}

int destroy_uart() {
    delete uart_str_stream;
}
