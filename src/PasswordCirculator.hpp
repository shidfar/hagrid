#ifndef PASSWORD_CIRCULATOR_H
    #define PASSWORD_CIRCULATOR_H
    #include <stdio.h>
    #include <string.h>

    #include "Circle.hpp"
    #define PSWD_NUM 5

    namespace hagrid {
        class PasswordCirculator
        {
        private:
            Circle * circle;
            char * names[PSWD_NUM] = {
                "name-one",
                "name-two",
                "name-three",
                "name-four",
                "name-five"
            };
            char * values[PSWD_NUM] = {
                "password-one",
                "password-two",
                "password-three",
                "password-four",
                "password-five"
            };
            void initialize_circle();
        public:
            PasswordCirculator();
            ~PasswordCirculator();
            void next();
            void prev();
            char * current_password();
            char * current_name();
            char * current_page_top_to_botom(uint8_t);
            char * current_page_bottom_to_top(uint8_t);
        };
    }
#endif
