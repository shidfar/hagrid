#ifndef HAGRID_GLOBALDEFS_H
    #define HAGRID_GLOBALDEFS_H
    
    #define LCD_W 16
    #define LCD_H 2
    #define LED 17 // Just for testing if needed!

    #define PAGE_SIZE 4

    #define SCREEN_WIDTH 128 // OLED display width, in pixels
    #define SCREEN_HEIGHT 32 // OLED display height, in pixels
    // Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
    // The pins for I2C are defined by the Wire-library. 
    // On an arduino UNO:       A4(SDA), A5(SCL)
    // On an arduino MEGA 2560: 20(SDA), 21(SCL)
    // On an arduino LEONARDO:   2(SDA),  3(SCL), ...
    #define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
    #define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
    
    #define DOWN_BUTTON   15
    #define UP_BUTTON   14
    #define OK_BUTTON 16
    
    // #define RE_SW 15
    // #define RE_DT 14
    // #define RE_CLK 16

#endif
// 