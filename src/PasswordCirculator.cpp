#include "PasswordCirculator.hpp"

namespace hagrid
{
    PasswordCirculator::PasswordCirculator() {
        this->initialize_circle();
    }
    
    PasswordCirculator::~PasswordCirculator() {
    }

    void PasswordCirculator::initialize_circle() {
        this->circle = new Circle(0);
        Circle * first;
        first = circle;
        for (uint32_t i = 1; i < PSWD_NUM; i ++) {
            Circle * next = new Circle(i);
            next->prev = circle;
            circle->next = next;
            circle = next;
        }
        // close the loop
        first->prev = circle;
        circle->next = first;
        circle = circle->next;
    }

    void PasswordCirculator::next() {
        circle = circle->next;
    }

    void PasswordCirculator::prev() {
        circle = circle->prev;
    }

    char * PasswordCirculator::current_password() {
        return values[circle->idx];
    }

    char * PasswordCirculator::current_name() {
        return names[circle->idx];
    }

    // top_to_botom
    char * PasswordCirculator::current_page_top_to_botom(uint8_t page_size) {
        // First, calculate the total length needed for the response string.
        size_t total_length = 0;
        Circle* current = circle;
        for (uint8_t i = 0; i < page_size; i++) {
            total_length += strlen(names[current->idx]) + 3; // +1 for the newline character +2 for indentation
            current = current->next;
        }
        total_length += 1; // +1 for the null terminator

        // Allocate memory for the response string.
        char* response = new char[total_length];
        if (!response) {
            // Handle memory allocation failure if needed.
            return nullptr;
        }

        // Initialize the response string to an empty string.
        response[0] = '\0';

        // Concatenate the names into the response string.
        current = circle;
        for (uint8_t i = 0; i < page_size; i++) {
            strcat(response, "  ");
            strcat(response, names[current->idx]);
            strcat(response, "\n");
            current = current->next;
        }

        return response;
    }

    // botom_to_top
    char * PasswordCirculator::current_page_bottom_to_top(uint8_t page_size) {
        // First, calculate the total length needed for the response string.
        size_t total_length = 0;
        Circle* current = circle;
        for (uint8_t i = 0; i < page_size; i++) {
            total_length += strlen(names[current->idx]) + 3; // +1 for the newline character +2 for indentation
            current = current->prev;
        }
        total_length += 1; // +1 for the null terminator

        // Allocate memory for the response string.
        char* response = new char[total_length];
        if (!response) {
            // Handle memory allocation failure if needed.
            return nullptr;
        }

        // Initialize the response string to an empty string.
        response[0] = '\0';

        // Concatenate the names into the response string.
        // current = circle;
        for (uint8_t i = 0; i < page_size; i++) {
            current = current->next;
            strcat(response, "  ");
            strcat(response, names[current->idx]);
            strcat(response, "\n");
        }

        return response;
    }
} // namespace hagrid
