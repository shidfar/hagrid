/**
 * All these macros has to be defined in the makefile and
 * processed during the make process
 * */
#ifndef NODEVDEFS
    #ifndef HAGRID_DEVDEFS_H
        #define HAGRID_DEVDEFS_H
        #ifndef ARDUINO
            #define ARDUINO 10809
        #endif
        
        // this has to be already defined during the make process
        #ifndef __AVR_ATmega32U4__
            #define __AVR_ATmega32U4__
        #endif

        // actual baud has to be defined during the make process
        #ifndef BAUD
            #define BAUD  9600 // 115200 
        #endif

        // this flag has to be defined during the make process
        #ifndef _USING_HID
            #define _USING_HID
        #endif
    #endif
#endif