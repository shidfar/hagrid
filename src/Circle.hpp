#ifndef CIRCLE_H
    #define CIRCLE_H

    #include <stdio.h>

    struct Circle {
        Circle(uint8_t val) : idx(val) {} 
        uint8_t idx;
        Circle * next;
        Circle * prev;
    };

#endif