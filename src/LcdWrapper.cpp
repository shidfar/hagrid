#include "LcdWrapper.hpp"

namespace hagrid {
    LcdWrapper::LcdWrapper() {
        this->oled = new Adafruit_SSD1306(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
        if(!this->oled->begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
            // Serial.println(F("SSD1306 allocation failed"));
            for(;;); // Don't proceed, loop forever
        }

        this->reset();
    }
    
    LcdWrapper::~LcdWrapper() {
        delete oled;
    }

    void LcdWrapper::println(char *str) {
        uint8_t max_line = 18;
        uint8_t len = min(strlen(str), max_line);

        this->oled->write(' '); this->oled->write(' ');

        for(uint8_t i = 0; i < len; i++) {
            this->oled->write(str[i]);
        }
        this->oled->write('\n');

        this->oled->display();
    }

    void LcdWrapper::print(char *src) {
        uint8_t len = strlen(src);

        for (uint8_t i = 0; i < len; i++) {
            this->oled->write(src[i]);
        }

        this->oled->display();
    }

    void LcdWrapper::mark_line(uint8_t line_no) {
        uint8_t cur = line_no * 8;
        uint8_t x = 0, y = cur;

        for (y = 0; y < 4; y++)
            for (x = 0; x <= y; x++)
                this->oled->drawPixel(x, cur + y, SSD1306_WHITE);

        for (; y < 8; y++)
            for (x = 0; x <= 7 - y; x++)
                this->oled->drawPixel(x, cur + y, SSD1306_WHITE);
        
        this->oled->display();
    }

    void LcdWrapper::clear_mark(uint8_t line_no) {
        uint8_t cur = line_no * 8;
        uint8_t x = 0, y = cur;

        for (y = 0; y < 4; y++)
            for (x = 0; x <= y; x++)
                this->oled->drawPixel(x, cur + y, SSD1306_BLACK);

        for (; y < 8; y++)
            for (x = 0; x <= 7 - y; x++)
                this->oled->drawPixel(x, cur + y, SSD1306_BLACK);
        
        this->oled->display();
    }

    void LcdWrapper::reset() {
        this->oled->clearDisplay();

        this->oled->setTextSize(1);      // Normal 1:1 pixel scale
        this->oled->setTextColor(SSD1306_WHITE); // Draw white text
        this->oled->setCursor(0, 0);     // Start at top-left corner
        this->oled->cp437(true);         // Use full 256 char 'Code Page 437' font

        // Not all the characters will fit on the display. This is normal.
        // Library will draw what it can and the rest will be clipped.
        this->oled->display();
    }
}
