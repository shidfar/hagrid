#ifndef EXECUTION_ENGINE_H
    #define EXECUTION_ENGINE_H

    #include "DevDefs.h"
    #include "GlobalDefs.h"

    #include <stdio.h>
    #include <util/delay.h>
    #include <avr/io.h>
    #include <util/setbaud.h>

    #include "LcdWrapper.hpp"
    #include "KeyboardHid.hpp"
    #include "PasswordCirculator.hpp"

    namespace hagrid {
        class ExecutionEngine
        {
        private:
            inline int routine(int);
            LcdWrapper *lcd_screen;
            KeyboardHid *hid;
            PasswordCirculator *passworder;

        public:
            ExecutionEngine();
            ~ExecutionEngine();
            void run(void);
            void rerender_page(bool);
        };
    };
#endif