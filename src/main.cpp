#include "uart.hpp"
#include <Arduino.h>
#include "ExecutionEngine.hpp"

// Declared weak in Arduino.h to allow user redefinitions.
int atexit(void (*)()) {return 0;}

// Weak empty variant initialization function.
// May be redefined by variant files.
void initVariant() __attribute__((weak));
void initVariant() {}

void setupUSB() __attribute__((weak));
void setupUSB() {}
void keepalive(void);

int main() {
    init();
    initVariant();
    initialize_uart();

    #if defined(USBCON)
        USBDevice.attach();
    #endif

    hagrid::ExecutionEngine * engine = new hagrid::ExecutionEngine();
    engine->run();

    keepalive(); // never let it return
    delete engine;
    destroy_uart();
    return 0;
}

void keepalive(void) {
    while (true) {
        _delay_ms(1000);
    }
}
