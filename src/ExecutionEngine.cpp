#include "ExecutionEngine.hpp"

namespace hagrid
{
    ExecutionEngine::ExecutionEngine() {
        lcd_screen = new LcdWrapper();
        hid = new KeyboardHid();
        passworder = new PasswordCirculator();
        
        pinMode(DOWN_BUTTON, INPUT_PULLUP);
        pinMode(UP_BUTTON, INPUT_PULLUP);
        pinMode(DOWN_BUTTON, INPUT_PULLUP);
        pinMode(LED, OUTPUT);
    }
    
    int ExecutionEngine::routine(int current_line) {
        
        if (digitalRead(DOWN_BUTTON) == LOW) {
            current_line += 1;
            this->passworder->next();
            if (current_line >= PAGE_SIZE) {
                current_line = 0;
            }
            return current_line;
        }

        if (digitalRead(UP_BUTTON) == LOW) {
            current_line -= 1;
            this->passworder->prev();
            if (current_line < 0) {
                current_line = PAGE_SIZE - 1;
            }
            return current_line;
        }

        if (digitalRead(OK_BUTTON) == LOW) {
            this->hid->type_in(this->passworder->current_password());
        }

        return current_line;
    }

    void ExecutionEngine::rerender_page(bool top_to_botom) {
        this->lcd_screen->reset();
        char * scr;
        if (top_to_botom) {
            scr = this->passworder->current_page_top_to_botom(PAGE_SIZE);
        } else {
            scr = this->passworder->current_page_bottom_to_top(PAGE_SIZE);
        }
        this->lcd_screen->print(scr);
        delete scr;
    }

    void ExecutionEngine::run(void) {
        int current_line = 0;
        int trace = current_line;
        char* src;
        src = this->passworder->current_page_top_to_botom(PAGE_SIZE);
        this->lcd_screen->print(src);
        this->lcd_screen->mark_line(current_line);
        delete src;

        while (true) {
            trace = current_line;
            current_line = routine(current_line);

            if (trace != current_line) {
                if (current_line == 0) {
                    rerender_page(true);
                }

                if (current_line == (PAGE_SIZE - 1)) {
                    rerender_page(false);
                }

                this->lcd_screen->clear_mark(trace);
                this->lcd_screen->mark_line(current_line);
                _delay_ms(300);
            }
        }
    }

    ExecutionEngine::~ExecutionEngine() {
        delete lcd_screen;
        delete hid;
        delete passworder;
    }
    
} // namespace hagrid
