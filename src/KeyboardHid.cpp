#include "KeyboardHid.hpp"

namespace hagrid {
    KeyboardHid::KeyboardHid() {
    }
    
    KeyboardHid::~KeyboardHid() {
    }

    void KeyboardHid::type_in(char * keys) {
        uint16_t len = strlen(keys);

        for (uint16_t i = 0; i < len; i++) {
            Keyboard.write(keys[i]);
            delay(87);
        }
    }
}